import pandas as pd
import matplotlib.pyplot as plt
import sys

def divide_dataframes_by_floor_component(dataframe):
    return dict(tuple(dataframe.groupby(['Floor', 'Component', 'Type'])))


def plot_chart(key, dataframe):
    value_list = dataframe["Value"].tolist()
    timestamp_list = dataframe["Timestamp"].tolist()

    tmp = timestamp_list[0]

    timestamp_list = list(map(lambda x: (x - tmp) / 1000, timestamp_list))

    plt.title(f"{key[2]}")
    plt.xlabel("Timestamp")
    plt.ylabel("Value")

    plt.plot(timestamp_list, value_list)
    plt.savefig(f"src/main/resources/{key[0]}{key[1]}{key[2]}.png")
    plt.clf()


def make_chart(floor_id: str, component_id: str, actuators_splitted):
    for key, dataframe in actuators_splitted.items():
        if floor_id == key[0] and component_id == key[1]:
            plot_chart(key, dataframe)


def main():
    if len(sys.argv) != 3:
        print("Insert 2 value: [floorId] [componentId]")
        exit(1)

    floor_id = sys.argv[1]
    component_id = sys.argv[2]

    actuators_filename = "actuators.csv"
    actuators_splitted = divide_dataframes_by_floor_component(pd.read_csv(actuators_filename))

    make_chart(floor_id, component_id, actuators_splitted)

    exit(0)


if __name__ == "__main__":
    main()