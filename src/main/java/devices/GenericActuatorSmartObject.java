package devices;

import emulated_devices.EmulatedActuator;
import emulated_devices.SmartObjectResource;
import message.ControlMessage;
import model.ActuatorDescriptor;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Generic Actuator, with a full implementation of the Actuation. Used for parent class in the other Actuator
 * objects only for modify Variables in base of Type and Unit of the actuator
 */
public abstract class GenericActuatorSmartObject extends GenericSmartObject<ActuatorDescriptor> {
    private static final Logger logger = LoggerFactory.getLogger(GenericActuatorSmartObject.class);

    protected static final String COMPONENT_TOPIC = "component";

    //protected static final String DEVICE_TOPIC = "device";

    protected static final String CONTROL_TOPIC = "control";

    protected String floorId;

    protected String componentId;

    private SmartObjectResource<ActuatorDescriptor> smartObjectResource;

    protected static String ACTUATOR_TYPE;

    protected static String TYPE;

    /**
     * Constructor
     */
    public GenericActuatorSmartObject() {
        super();
    }

    /**
     * Init function to initialize values
     * @param floorId String that represent the floorId of a Floor
     * @param componentId String that represent the componentId of a Device
     * @param iMqttClient Client used to publish
     * @param smartObjectResource Resource used to take data (In this case Emulated Actuator)
     */
    public void init(String floorId, String componentId, IMqttClient iMqttClient, SmartObjectResource<ActuatorDescriptor> smartObjectResource) {
        this.floorId = floorId;
        this.componentId = componentId;
        this.iMqttClient = iMqttClient;
        this.smartObjectResource = smartObjectResource;
    }

    /**
     * start and initialize a Listener, that every time the SmartObjectResource notify this object,
     * publish the telemetry data on the actuator topic
     */
    public void start() {
        this.smartObjectResource.addDataListener(((resource, updatedValue) -> {
            if (resource != null && updatedValue != null) {
                try {
                    publishTelemetryData(String.format("%s/%s/%s/%s/%s", BASE_TOPIC, floorId, COMPONENT_TOPIC, componentId, ACTUATOR_TYPE),
                            makeTelemetryMessage(TYPE, updatedValue));
                } catch (MqttException e) {
                    logger.error(e.getMessage());
                }
            }
        }));

        registerToControlTopic();
    }

    /**
     * Register to control topic of the actuator to Turn On or Off
     */
    private void registerToControlTopic() {
        try {
            this.iMqttClient.subscribe(String.format("%s/%s/%s/%s/%s/%s", BASE_TOPIC, floorId, COMPONENT_TOPIC, componentId, ACTUATOR_TYPE, CONTROL_TOPIC), (topic, msg) -> {
                if (msg != null) {
                    byte[] payload = msg.getPayload();

                    ControlMessage message = this.objectMapper.readValue(new String(payload), ControlMessage.class);

                    checkControlMessage(message);
                }
            });
        } catch (MqttException e) {
            logger.error("Cannot subscribe to Control topic: {}", e.getMessage());
        }
    }

    /**
     * Control if the message Turn On or Off the actuator and change the "State"
     * @param message ControlMessage which contain the information to Turn On or Off the actuator
     */
    private void checkControlMessage(ControlMessage message) {

        logger.info("Checking control Message for turn off or on the actuator {} in {}", this.componentId, message.getIsActive());

        ((EmulatedActuator) this.smartObjectResource)
                .getActuatorDescriptor()
                .getDeviceDescriptor()
                .setEnabled(message.getIsActive());
    }
}