package devices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import message.TelemetryMessage;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generic smart object used for Sensoring. Abstract class that manages telemetry data.
 * @param <T> ActuatorDescriptor or PeopleSensorDescriptor
 */
public abstract class GenericSmartObject<T> {

    private static final Logger logger = LoggerFactory.getLogger(GenericSmartObject.class);

    protected static final String BASE_TOPIC = "floor";

    protected static final int QOS = 0;

    protected IMqttClient iMqttClient;

    protected ObjectMapper objectMapper;

    /**
     * Constructor
     */
    public GenericSmartObject() {
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Convert a TelemetryMessage class to Json (Generic)
     * @param telemetryMessage Telemetry message to convert in Json
     * @return Json converted in to String
     */
    protected String convertoToJson(TelemetryMessage<T> telemetryMessage) {
        try {
            return this.objectMapper.writeValueAsString(telemetryMessage);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
            return "";
        }
    }

    /**
     * Create a TelemetryMessage for telemetry reason.
     * @param type The type of the message
     * @param descriptor ActuatorDescriptor or PeopleSensorDescriptor (in this case) to insert in the telemetry message
     * @return A newly created TelemetryMessage
     */
    protected TelemetryMessage<T> makeTelemetryMessage(String type, T descriptor) {
        return new TelemetryMessage<T>(type, System.currentTimeMillis(), descriptor);
    }

    /**
     * Publish to topic a Telemetry Message
     * @param topic MQTT topic to publish the TelemetryMessage
     * @param telemetryMessage a TelemetryMessage to publish
     * @throws MqttException if the MqttClient is having a problem in connection
     */
    protected void publishTelemetryData(String topic, TelemetryMessage<T> telemetryMessage) throws MqttException {
        if (this.iMqttClient != null && this.iMqttClient.isConnected() && telemetryMessage != null && topic != null) {
            String jsonDataPayload = this.convertoToJson(telemetryMessage);

            MqttMessage mqttMessage = new MqttMessage(jsonDataPayload.getBytes());
            mqttMessage.setQos(QOS);

            iMqttClient.publish(topic, mqttMessage);

            logger.info("Data Correctly Published to topic: {}", topic);
        }
        else {
            logger.error("Error: Topic or MSg = null or MQTT Client is not connected!");
        }
    }
}