package devices;

import emulated_devices.EmulatedPeopleSensor;
import emulated_devices.SmartObjectResource;
import model.PeopleSensorDescriptor;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * People sensor used to count how many people are inside a specific Floor
 */
public class PeopleSmartObject extends GenericSmartObject<PeopleSensorDescriptor> {
    private static final Logger logger = LoggerFactory.getLogger(PeopleSmartObject.class);

    private static final String TYPE = "iot:snbld:people-sensor";

    private static final String PEOPLE_SENSOR_TOPIC = "people-sensor";

    private String floorId;

    private SmartObjectResource<PeopleSensorDescriptor> smartObjectResource;

    /**
     * Constructor
     */
    public PeopleSmartObject() {
        super();
    }

    /**
     * Init function used to initialize the People Smart Object
     * @param floorId String that represent the floorId of a Floor
     * @param iMqttClient Client used to publish
     * @param smartObjectResource Resource used to take data (In this case Emulated Actuator)
     */
    public void init(String floorId, IMqttClient iMqttClient, SmartObjectResource<PeopleSensorDescriptor> smartObjectResource) {
        this.floorId = floorId;
        this.iMqttClient = iMqttClient;
        this.smartObjectResource = smartObjectResource;
    }

    /**
     * start and initialize a Listener, that every time the SmartObjectResource notify this object,
     * publish the telemetry data on the people Sensor topic
     */
    public void start() {
        this.smartObjectResource.addDataListener((resource, updatedValue) -> {
            if (resource != null && updatedValue != null) {
                try {
                    publishTelemetryData(String.format("%s/%s/%s", BASE_TOPIC, floorId, PEOPLE_SENSOR_TOPIC),
                            makeTelemetryMessage(TYPE, updatedValue));
                } catch (MqttException e) {
                    logger.error(e.getMessage());
                }
            }
        });
    }

    public static void main(String[] args) throws Exception {
        final String MQTT_BROKER_IP = "127.0.0.1";

        final int MQTT_BROKER_PORT = 1883;

        if (args.length != 1) {
            logger.error("Insert 1 Value: FloorId");
            System.exit(1);
        }

        String floorId = args[0];

        MqttClientPersistence persistence = new MemoryPersistence();
        IMqttClient mqttClient = new MqttClient(String.format("tcp://%s:%d", MQTT_BROKER_IP, MQTT_BROKER_PORT), floorId, persistence);

        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);

        mqttClient.connect(options);

        logger.info("MQTT Client Connected! Client Id: {}", floorId);

        PeopleSmartObject peopleSmartObject = new PeopleSmartObject();

        EmulatedPeopleSensor emulatedPeopleSensor = new EmulatedPeopleSensor(floorId);

        peopleSmartObject.init(floorId, mqttClient, emulatedPeopleSensor);

        peopleSmartObject.start();
    }
}