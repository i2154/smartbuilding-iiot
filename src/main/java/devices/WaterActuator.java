package devices;

import emulated_devices.EmulatedActuator;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * Water Actuator, the only change make to respect of the Parent class is the ActuatorType and Type
 */
public class WaterActuator extends GenericActuatorSmartObject {
    private static final Logger logger = LoggerFactory.getLogger(WaterActuator.class);

    /**
     * Constructor
     */
    public WaterActuator() {
        ACTUATOR_TYPE = "water";
        TYPE = "iot:snbld:water-actuator";
    }

    public static void main(String[] args) throws Exception {
        final String MQTT_BROKER_IP = "127.0.0.1";

        final int MQTT_BROKER_PORT = 1883;

        if (args.length != 2) {
            logger.error("Insert 2 Values: FloorId ComponentId");
            System.exit(1);
        }

        String floorId = args[0];

        String componentId = args[1];

        String uuid = UUID.randomUUID().toString();

        MqttClientPersistence persistence = new MemoryPersistence();
        IMqttClient mqttClient = new MqttClient(String.format("tcp://%s:%d", MQTT_BROKER_IP, MQTT_BROKER_PORT),
                uuid, persistence);

        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);

        mqttClient.connect(options);

        logger.info("MQTT Client Connected! Client Id: {}", uuid);

        WaterActuator waterActuator = new WaterActuator();

        EmulatedActuator emulatedActuator = new EmulatedActuator(floorId, componentId, uuid, "l/s");

        waterActuator.init(floorId, componentId, mqttClient, emulatedActuator);

        waterActuator.start();
    }
}