package client;

import message.TelemetryMessage;
import model.ActuatorDescriptor;
import model.PeopleSensorDescriptor;
import model.PolicyDescriptor;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.HttpServer;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The "Main" class that manages all the Actuators and people Sensors, implement Policies
 */
public class SmartbuildingClient {
    private static final Logger logger = LoggerFactory.getLogger(SmartbuildingClient.class);

    private ClientHelper clientHelper;

    public static final int CALCULATION_INTERVAL = 60;

    private final MqttHelper mqttHelper;

    HashMap<ImmutableTriple<String, String, String>, ActuatorDescriptor> devices;
    HashMap<ImmutableTriple<String, String, String>, PolicyDescriptor> policies;
    HashMap<String, PeopleSensorDescriptor> peopleSensors;

    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);

    /**
     * Constructor
     *
     * @param mqttHelper The MqttHelper class to interface with
     */
    public SmartbuildingClient(MqttHelper mqttHelper) {
        this.mqttHelper = mqttHelper;

        this.clientHelper = new ClientHelper(this);

        this.devices = new HashMap<>();
        this.policies = new HashMap<>();
        this.peopleSensors = new HashMap<>();
    }

    /**
     * Init Function to initialize the DataListeners, and implement the Client insert or update the values of
     * Actuator and peopleSensor. In the people sensor part, not only update the values but call the functions
     * that implement Policy (Turn On or Off Actuators)
     */
    public void init() {
        this.mqttHelper.telemetryActuatorNotification.addDataListener((resource, updatedValue) -> {
            if (resource != null && updatedValue != null) {
                logger.info("Notifaction of Actuator: {}", updatedValue.toString());

                insertOrUpdateDeviceMap(updatedValue);
            }
        });

        this.mqttHelper.peopleSensorNotification.addDataListener((resource, updatedValue) -> {
            if (resource != null && updatedValue != null) {
                logger.info("Notifaction of PeopleSensors: {}", updatedValue.toString());

                // Create or Update People Sensors in memory
                insertOrUpdatePeopleSensorMap(updatedValue);

                // If Floor is empty turn off all devices if Exceed maximum consume setted in the policy, and
                // set a timer for reactivate after the seconds setted in the policy for this specific device,
                // and if is not possible set a FLAG for reactivate the sensor after the people sensor
                // count is over 0
                checkIfFloorIsEmptyAndTurnOff(updatedValue);

                //Turn on Devices with timer finished with the FLAG timerFinished setted ON
                turnOnAllDevicesOnFloor(updatedValue);
            }
        });
    }

    /**
     * Check the Telemetry Message and decide if all Actuators on the floor must turn On.
     * If the timer is expired re-turn On the actuator and reset the timer Boolean
     *
     * @param message Telemetry message
     */
    private void turnOnAllDevicesOnFloor(TelemetryMessage<PeopleSensorDescriptor> message) {
        if (message.getDataValue().getCurrentPeople() == 1) {
            LinkedList<ActuatorDescriptor> sameFloorDevices = getDevicesOnSameFloor(message.getDataValue().getFloorId());

            sameFloorDevices.forEach(actuatorDescriptor -> logger.info("List of Devices on same floor: {}", actuatorDescriptor.getDeviceDescriptor().toString()));

            sameFloorDevices.stream().filter(actuatorDescriptor -> this.policies.get(new ImmutableTriple<>(actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                    actuatorDescriptor.getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(actuatorDescriptor.getUnit()))).getTimerFinished()).forEach(actuatorDescriptor -> {

                this.mqttHelper.turnOnOffActuator(actuatorDescriptor, true);
                this.policies.get(new ImmutableTriple<>(actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                        actuatorDescriptor.getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(actuatorDescriptor.getUnit()))).setTimerFinished(false);
            });
        }
    }

    /**
     * Check if a floor is empty and if yes, call the other function on all devices in the same floor
     *
     * @param message Telemetry message to check (People sensor descriptor)
     */
    private void checkIfFloorIsEmptyAndTurnOff(TelemetryMessage<PeopleSensorDescriptor> message) {
        if (message.getDataValue().getCurrentPeople() == 0) {
            LinkedList<ActuatorDescriptor> sameFloorDevices = getDevicesOnSameFloor(message.getDataValue().getFloorId());

            turnOffIfDeviceExceedMaxConsume(sameFloorDevices);
        }
    }

    /**
     * Construct a LinkedList with all the devices in the same floor
     *
     * @param floorId String that represent the floorId of a Floor
     * @return LinkedList with all devices in the same floor
     */
    private LinkedList<ActuatorDescriptor> getDevicesOnSameFloor(String floorId) {
        LinkedList<ActuatorDescriptor> sameFloorDevices = new LinkedList<>();

        if (floorId != null) {
            this.devices.forEach((stringStringImmutableTriple, actuatorDescriptor) -> {
                if (floorId.equals(actuatorDescriptor.getDeviceDescriptor().getFloorId())) {
                    sameFloorDevices.push(actuatorDescriptor);
                }
            });
        }

        return sameFloorDevices;
    }

    /**
     * Turn off devices in the same floor, if the policy is enabled and exceed max consume.
     * After the turn off, start a timer of second setted in the policy descriptor for reactivate
     * the actuator.
     *
     * @param sameFloorDevices LinkedList with devices on the same Floor
     */
    private void turnOffIfDeviceExceedMaxConsume(LinkedList<ActuatorDescriptor> sameFloorDevices) {
        sameFloorDevices.forEach(actuatorDescriptor -> {
            if (actuatorDescriptor.getValue() >= this.policies.get(new ImmutableTriple<>(actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                    actuatorDescriptor.getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(actuatorDescriptor.getUnit()))).getMaxConsume()
                    && this.policies.get(new ImmutableTriple<>(actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                    actuatorDescriptor.getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(actuatorDescriptor.getUnit()))).getEnabled()
                    && actuatorDescriptor.getDeviceDescriptor().getEnabled()) {

                this.mqttHelper.turnOnOffActuator(actuatorDescriptor, false);

                // Set to OFF for policies things
                this.devices.get(new ImmutableTriple<>(actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                        actuatorDescriptor.getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(actuatorDescriptor.getUnit()))).getDeviceDescriptor().setEnabled(false);

                scheduler.schedule(() -> {
                    if (peopleSensors.get(actuatorDescriptor.getDeviceDescriptor().getFloorId()).getCurrentPeople() != 0) {
                        mqttHelper.turnOnOffActuator(actuatorDescriptor, true);
                    }
                    else {
                        policies.get(new ImmutableTriple<>(actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                                actuatorDescriptor.getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(actuatorDescriptor.getUnit()))).setTimerFinished(true);
                    }

                }, this.policies.get(new ImmutableTriple<>(actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                        actuatorDescriptor.getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(actuatorDescriptor.getUnit()))).getTimeToReactivate(), TimeUnit.SECONDS);
            }
        });
    }

    /**
     * Update the People Sensor Map with new Values
     *
     * @param updatedValue Telemetry message for Insert or Update the People sensor data
     */
    private void insertOrUpdatePeopleSensorMap(TelemetryMessage<PeopleSensorDescriptor> updatedValue) {
        if (isNewPeopleSensor(updatedValue.getDataValue())) {
            logger.info("New People Sensor found: floor: {}", updatedValue.getDataValue().getFloorId());

            this.peopleSensors.put(updatedValue.getDataValue().getFloorId(), updatedValue.getDataValue());
        }
        else {
            logger.info("Updated PeopleSensor: floor: {}", updatedValue.getDataValue().getFloorId());

            this.peopleSensors.replace(updatedValue.getDataValue().getFloorId(), updatedValue.getDataValue());
        }
    }

    /**
     * Update the Actuator Sensor Map with new Values
     *
     * @param updatedValue Telemetry message for Insert or Update the Actuator sensor data
     */
    private void insertOrUpdateDeviceMap(TelemetryMessage<ActuatorDescriptor> updatedValue) {
        if (isNewActuator(updatedValue.getDataValue())) {
            logger.info("New Actuator found: floor: {}  component: {}  type: {}", updatedValue.getDataValue().getDeviceDescriptor().getFloorId(),
                    updatedValue.getDataValue().getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(updatedValue.getDataValue().getUnit()));

            this.devices.put(new ImmutableTriple<>(updatedValue.getDataValue().getDeviceDescriptor().getFloorId(),
                    updatedValue.getDataValue().getDeviceDescriptor().getComponentId(), updatedValue.getType()), updatedValue.getDataValue());

            this.policies.put(new ImmutableTriple<>(updatedValue.getDataValue().getDeviceDescriptor().getFloorId(),
                    updatedValue.getDataValue().getDeviceDescriptor().getComponentId(), updatedValue.getType()), new PolicyDescriptor());
        }
        else {
            logger.info("Actuator Updated: floor: {}  component: {}  type: {}", updatedValue.getDataValue().getDeviceDescriptor().getFloorId(),
                    updatedValue.getDataValue().getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(updatedValue.getDataValue().getUnit()));

            this.devices.replace(new ImmutableTriple<>(updatedValue.getDataValue().getDeviceDescriptor().getFloorId(),
                    updatedValue.getDataValue().getDeviceDescriptor().getComponentId(), updatedValue.getType()), updatedValue.getDataValue());
        }
    }

    private Boolean isNewPeopleSensor(PeopleSensorDescriptor peopleSensorDescriptor) {
        return !this.peopleSensors.containsKey(peopleSensorDescriptor.getFloorId());
    }

    private Boolean isNewActuator(ActuatorDescriptor actuatorDescriptor) {
        return !this.devices.containsKey(new ImmutableTriple<>(actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                actuatorDescriptor.getDeviceDescriptor().getComponentId(), MqttHelper.unitToType(actuatorDescriptor.getUnit())));

    }

    public HashMap<ImmutableTriple<String, String, String>, ActuatorDescriptor> getDevices() {
        return devices;
    }

    public void setDevices(HashMap<ImmutableTriple<String, String, String>, ActuatorDescriptor> devices) {
        this.devices = devices;
    }

    public HashMap<String, PeopleSensorDescriptor> getPeopleSensors() {
        return peopleSensors;
    }

    public void setPeopleSensors(HashMap<String, PeopleSensorDescriptor> peopleSensors) {
        this.peopleSensors = peopleSensors;
    }

    public ClientHelper getClientHelper() {
        return clientHelper;
    }

    public void setClientHelper(ClientHelper clientHelper) {
        this.clientHelper = clientHelper;
    }

    public HashMap<ImmutableTriple<String, String, String>, PolicyDescriptor> getPolicies() {
        return policies;
    }

    public void setPolicies(HashMap<ImmutableTriple<String, String, String>, PolicyDescriptor> policies) {
        this.policies = policies;
    }

    public static void main(String[] args) throws MqttException {
        String BROKER_ADDRESS = "127.0.0.1";
        int BROKER_PORT = 1883;

        String clientId = UUID.randomUUID().toString();

        MqttClientPersistence persistence = new MemoryPersistence();

        IMqttClient mqttClient = new MqttClient(String.format("tcp://%s:%d", BROKER_ADDRESS, BROKER_PORT), clientId, persistence);

        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);

        mqttClient.connect(options);

        logger.info("Connected! Client Id: {}", clientId);

        MqttHelper mqttHelper = new MqttHelper(mqttClient);
        mqttHelper.init();

        SmartbuildingClient smartbuildingClient = new SmartbuildingClient(mqttHelper);
        smartbuildingClient.init();

        HttpServer httpServer = new HttpServer(smartbuildingClient.getClientHelper(), mqttHelper);
    }
}