package client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import emulated_devices.SmartObjectResource;
import message.ControlMessage;
import message.TelemetryMessage;
import model.ActuatorDescriptor;
import model.PeopleSensorDescriptor;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used by the SmartbuildingClient to separate the Logic from the "Dirty" part of MQTT.
 * This approach can help to identify problems and errors, and isolate them
 */
public class MqttHelper {
    private static final Logger logger = LoggerFactory.getLogger(MqttHelper.class);

    private static final String BASE_TOPIC = "floor";

    private static final String COMPONENT_TOPIC = "component";

    private static final String PEOPLE_SENSOR_TOPIC = "people-sensor";

    private static final String CONTROL_TOPIC = "control";

    private static final int QOS = 0;

    private final IMqttClient mqttClient;

    private static ObjectMapper objectMapper;

    public final TelemetryActuatorNotification telemetryActuatorNotification = new TelemetryActuatorNotification();

    public final PeopleSensorNotification peopleSensorNotification = new PeopleSensorNotification();

    /**
     * Class used only for notify the client about actuator telemetry
     */
    protected static class TelemetryActuatorNotification extends SmartObjectResource<TelemetryMessage<ActuatorDescriptor>> {
        @Override
        public TelemetryMessage<ActuatorDescriptor> loadUpdatedValue() {
            return null;
        }
    }

    /**
     * Class used only for notify the client about people sensor telemetry
     */
    protected static class PeopleSensorNotification extends SmartObjectResource<TelemetryMessage<PeopleSensorDescriptor>> {
        @Override
        public TelemetryMessage<PeopleSensorDescriptor> loadUpdatedValue() {
            return null;
        }
    }

    /**
     * Constructor
     * @param mqttClient Mqtt Client
     */
    public MqttHelper(IMqttClient mqttClient) {
        this.mqttClient = mqttClient;
        objectMapper = new ObjectMapper();

        init();
    }

    /**
     * Init method
     */
    public void init() {
        registerToActuatorInfo();
        registerToPeopleSensor();
    }

    /**
     * Register to all Actuator topics with wildcards, and notify client about every update received
     */
    public void registerToActuatorInfo() {
        try {
            this.mqttClient.subscribe(String.format("%s/+/%s/#", BASE_TOPIC, COMPONENT_TOPIC), (topic, msg) -> {
                if (msg != null) {
                    byte[] payload = msg.getPayload();

                    TelemetryMessage<ActuatorDescriptor> message = objectMapper.readValue(new String(payload), new TypeReference<TelemetryMessage<ActuatorDescriptor>>() {});

                    this.telemetryActuatorNotification.notifyUpdate(message);
                }
            });
        } catch (MqttException e) {
            logger.error("Cannot register to Actuator topics. Error msg: {}", e.getMessage());
        }
    }

    /**
     * Register to all People Sensor topics with wildcards, and notify client about every update received
     */
    public void registerToPeopleSensor() {
        try {
            this.mqttClient.subscribe(String.format("%s/+/%s", BASE_TOPIC, PEOPLE_SENSOR_TOPIC), (topic, msg) -> {
                if (msg != null) {
                    byte[] payload = msg.getPayload();

                    TelemetryMessage<PeopleSensorDescriptor> message = objectMapper.readValue(new String(payload), new TypeReference<TelemetryMessage<PeopleSensorDescriptor>>() {});

                    this.peopleSensorNotification.notifyUpdate(message);
                }
            });
        } catch (MqttException e) {
            logger.error("Cannot register to PeopleSensor topics. Error msg: {}", e.getMessage());
        }
    }

    /**
     * Publish to Control topic to Turn Off or On an actuator
     *
     * @param mqttClient Mqtt Client
     * @param topic Topic to publish
     * @param message Control Message
     * @throws MqttException For problems with Mqtt Client
     */
    public static void publishToControlTopic(IMqttClient mqttClient, String topic, ControlMessage message) throws MqttException {
        new Thread(() -> {
            try {
                if (mqttClient != null && mqttClient.isConnected() && topic != null && message != null) {
                    String jsonDataPayload = objectMapper.writeValueAsString(message);

                    MqttMessage mqttMessage = new MqttMessage(jsonDataPayload.getBytes());
                    mqttMessage.setQos(QOS);

                    mqttClient.publish(topic, mqttMessage);

                    logger.info("CONTROL MESSAGE CORRECTLY PUBLISHED");
                }
            } catch (Exception e) {
                logger.error("Error in control thread publish {}", e.getMessage());
            }
        }).start();
    }

    /**
     * Turn On or Off actuator, make the Message and call the publishToControlTopic Method
     *
     * @param actuatorDescriptor Actuator Descriptor to turn off
     * @param active On (True), Off (False)
     */
    public void turnOnOffActuator(ActuatorDescriptor actuatorDescriptor, Boolean active) {
        String topic = String.format("%s/%s/%s/%s/%s/%s", BASE_TOPIC, actuatorDescriptor.getDeviceDescriptor().getFloorId(),
                COMPONENT_TOPIC, actuatorDescriptor.getDeviceDescriptor().getComponentId(),
                MqttHelper.unitToActuatorType(actuatorDescriptor.getUnit()), CONTROL_TOPIC);

        ControlMessage message = new ControlMessage(System.currentTimeMillis(), active);

        try {
            logger.info("Publish control message to topic: {}", topic);

            publishToControlTopic(this.mqttClient, topic, message);
        } catch (MqttException e) {
            logger.error("Cannot publish to control topic {}. Error message: {}", topic, e.getMessage());
        }
    }

    /**
     * Static method to convert a unit in to actuator Type
     * @param unit Unit used (kWh, kg or l/s)
     * @return Actuator Type
     */
    public static String unitToActuatorType(String unit) {
        if ("kWh".equals(unit)) {
            return "electricity";
        }
        if ("kg".equals(unit)) {
            return "gas";
        }
        if("l/s".equals(unit)) {
            return "water";
        }
        return "";
    }

    /**
     * Static method to convert a unit in to Type
     * @param unit Unit used (kWh, kg or l/s)
     * @return Type
     */
    public static String unitToType(String unit) {
        if ("kWh".equals(unit)) {
            return "iot:snbld:electricity-actuator";
        }
        if ("kg".equals(unit)) {
            return "iot:snbld:gas-actuator";
        }
        if("l/s".equals(unit)) {
            return "iot:snbld:water-actuator";
        }
        return "";
    }
}