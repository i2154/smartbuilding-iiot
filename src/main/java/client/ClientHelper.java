package client;

import com.opencsv.CSVWriter;
import gui.FloorTab;
import model.ActuatorDescriptor;
import model.PeopleSensorDescriptor;
import model.PolicyDescriptor;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Helper class that offer a variety of Function to Interface with a GUI
 */
public class ClientHelper {
    private static final Logger logger = LoggerFactory.getLogger(ClientHelper.class);

    private double gasPrice, electricityPrice, waterPrice;

    private SmartbuildingClient smartbuildingClient;

    /**
     * Constructor
     * @param smartbuildingClient The client to attach with
     */
    public ClientHelper(SmartbuildingClient smartbuildingClient) {
        this.smartbuildingClient = smartbuildingClient;

        initPrice();
    }

    /**
     * Init the Price Values (deprecated)
     */
    private void initPrice() {
        this.gasPrice = 1.65; // Prezzo del metano preso da internet
        this.waterPrice = 0.00137; // Prezzo dell'acqua a litro
        this.electricityPrice = 0.2564; // Prezzo dell'elettricita per kWh
    }

    public Map<ImmutableTriple<String, String, String>, ActuatorDescriptor> getDevicesMap() {
        return Collections.unmodifiableMap(this.smartbuildingClient.getDevices());
    }

    public Map<String, PeopleSensorDescriptor> getPeopleSensorMap() {
        return Collections.unmodifiableMap(this.smartbuildingClient.getPeopleSensors());
    }

    public HashMap<ImmutableTriple<String, String, String>, PolicyDescriptor> getPoliciesHashMap() {
        return this.smartbuildingClient.getPolicies();
    }

    /**
     * For 60 seconds this method collect the values of the specified Actuators and calculate the mean value cost
     * of every Actuator
     *
     * @param floorId String that represent the floorId of a Floor
     * @param componentId String that represent the componentId of a Device
     * @param gasPrice Price of the Gas
     * @param waterPrice Price of the Water
     * @param elecPrice Price of the Electricity
     * @return Hashmap used to store mean values in base of the actuator type
     * @throws InterruptedException If this thread cannot sleep throw this exception
     */
    public HashMap<String, Double> calculateMeanValue(String floorId, String componentId, double gasPrice, double waterPrice, double elecPrice) throws InterruptedException {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        HashMap<ImmutableTriple<String, String, String>, ActuatorDescriptor> actuatorHM = this.smartbuildingClient.getDevices();

        HashMap<String, Double> meanValues = new HashMap<>();
        meanValues.put("iot:snbld:electricity-actuator", 0.0);
        meanValues.put("iot:snbld:gas-actuator", 0.0);
        meanValues.put("iot:snbld:water-actuator", 0.0);

        scheduler.scheduleAtFixedRate(() -> {
            meanValues.put("iot:snbld:electricity-actuator", meanValues.get("iot:snbld:electricity-actuator") + actuatorHM.get(new ImmutableTriple<>(floorId, componentId, "iot:snbld:electricity-actuator")).getValue());
            meanValues.put("iot:snbld:gas-actuator", meanValues.get("iot:snbld:gas-actuator") + actuatorHM.get(new ImmutableTriple<>(floorId, componentId, "iot:snbld:gas-actuator")).getValue());
            meanValues.put("iot:snbld:water-actuator", meanValues.get("iot:snbld:water-actuator") + actuatorHM.get(new ImmutableTriple<>(floorId, componentId, "iot:snbld:water-actuator")).getValue());

        }, 0, 5, TimeUnit.SECONDS);


        Thread.sleep(61000);

        scheduler.shutdown();

        meanValues.forEach((s, aDouble) -> {
            switch (s) {
                case "iot:snbld:electricity-actuator" -> meanValues.put(s, (aDouble / 60) * elecPrice);
                case "iot:snbld:gas-actuator" -> meanValues.put(s, (aDouble / 60) * gasPrice);
                case "iot:snbld:water-actuator" -> meanValues.put(s, (aDouble / 60) * waterPrice);
            }
        });

        return meanValues;

    }

    /**
     * Write to file the records of all People sensor smart Object
     *
     * @throws IOException if the program cannot write to file
     */
    public void writePeopleSensorDataToCsvFile() throws IOException {
        File peopleSensorFile = new File("people_sensor.csv");

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        FileWriter outputPeopleSensorFile = new FileWriter(peopleSensorFile);

        CSVWriter peopleSensorWriter = new CSVWriter(outputPeopleSensorFile);

        String[] peopleSensorHeader = {"Floor", "People-in", "People-out", "Current-people", "Timestamp"};

        peopleSensorWriter.writeNext(peopleSensorHeader);

        HashMap<String, PeopleSensorDescriptor> peopleSensors = this.smartbuildingClient.getPeopleSensors();

        // People Sensor
        scheduler.scheduleAtFixedRate(() -> {
            LinkedList<String[]> lines = new LinkedList<>();

            synchronized (this) {
                peopleSensors.forEach((s, peopleSensorDescriptor) -> {
                    String[] line = {peopleSensorDescriptor.getFloorId(), String.valueOf(peopleSensorDescriptor.getPeopleIn()),
                            String.valueOf(peopleSensorDescriptor.getPeopleOut()), String.valueOf(peopleSensorDescriptor.getCurrentPeople()),
                            String.valueOf(System.currentTimeMillis())};

                    lines.push(line);
                });
            }

            lines.forEach(peopleSensorWriter::writeNext);

        }, 0, 5, TimeUnit.SECONDS);

        scheduler.schedule(() -> {
            scheduler.shutdown();
            try {
                peopleSensorWriter.close();

                outputPeopleSensorFile.close();

            } catch (IOException e) {
                logger.error("Cannot Shutdown threads for writing the devices and people sensors records! Msg: {}", e.getMessage());
            }
        }, SmartbuildingClient.CALCULATION_INTERVAL + 1, TimeUnit.SECONDS);
    }

    /**
     * Write for 60 seconds the record of the actuators selected in the floortab in a CSV file.
     * After that, start the Python program to generate with Various Libraries, chart about consumes.
     *
     * @param floorTab FloorTab which called this method
     * @throws IOException Raised when this method or thread cannot write to file
     * @throws InterruptedException Raised when this method or thread cannot call the sleep function
     */
    public void writeActuatorDataToCsvFile(FloorTab floorTab) throws IOException, InterruptedException {
        File actuatorFile = new File("actuators.csv");

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);

        FileWriter outputActuatorFile = new FileWriter(actuatorFile);

        CSVWriter actuatorWriter = new CSVWriter(outputActuatorFile);

        String[] actuatorHeader = {"Floor", "Component", "Type", "Value", "Timestamp"};

        actuatorWriter.writeNext(actuatorHeader);

        HashMap<ImmutableTriple<String, String, String>, ActuatorDescriptor> actuators = this.smartbuildingClient.getDevices();

        // Actuator
        scheduler.scheduleAtFixedRate(() -> {
            LinkedList<String[]> lines = new LinkedList<>();

            synchronized (this) {
                actuators.forEach((stringStringImmutableTriple, actuatorDescriptor) -> {
                    String[] line = {actuatorDescriptor.getDeviceDescriptor().getFloorId(), actuatorDescriptor.getDeviceDescriptor().getComponentId(),
                            MqttHelper.unitToActuatorType(actuatorDescriptor.getUnit()), String.valueOf(actuatorDescriptor.getValue()),
                            String.valueOf(System.currentTimeMillis())};

                    lines.push(line);
                });
            }

            lines.forEach(actuatorWriter::writeNext);

        }, 0, 5, TimeUnit.SECONDS);

        //scheduler.schedule(scheduler::shutdown, SmartbuildingClient.CALCULATION_INTERVAL + 1, TimeUnit.SECONDS);

        Thread.sleep(61000);

        try {
            scheduler.shutdown();
            actuatorWriter.close();

            outputActuatorFile.close();

            ProcessBuilder processBuilder = new ProcessBuilder("/usr/bin/python", "/home/rhohen/Workspace/smartbuilding-iot/makechart.py", floorTab.getText(), floorTab.getSelectedComponent());

            Process process = processBuilder.start();

        } catch (IOException e) {
            logger.error("Cannot Shutdown threads for writing the devices and people sensors records! Msg: {}", e.getMessage());
        }
    }

    public double getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(double gasPrice) {
        this.gasPrice = gasPrice;
    }

    public double getElectricityPrice() {
        return electricityPrice;
    }

    public void setElectricityPrice(double electricityPrice) {
        this.electricityPrice = electricityPrice;
    }

    public double getWaterPrice() {
        return waterPrice;
    }

    public void setWaterPrice(double waterPrice) {
        this.waterPrice = waterPrice;
    }
}