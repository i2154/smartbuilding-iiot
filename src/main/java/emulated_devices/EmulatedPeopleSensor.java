package emulated_devices;

import model.PeopleSensorDescriptor;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Emulated People Sensor for demo purpose
 */
public class EmulatedPeopleSensor extends SmartObjectResource<PeopleSensorDescriptor>{
    private static final int MAX_PEOPLE = 10;
    private static final int START_PEOPLE = 2;
    private static final int MIN_PEOPLE = 0;

    private static final long UPDATE_PERIOD = 5000;
    private static final long TASK_DELAY_TIME = 5000;

    private PeopleSensorDescriptor peopleSensorDescriptor;

    private Random random = null;

    private Timer updateTimer = null;

    /**
     * Constructor
     * @param floorId String that represent the floorId of a Floor
     */
    public EmulatedPeopleSensor(String floorId) {
        this.peopleSensorDescriptor = new PeopleSensorDescriptor();
        this.peopleSensorDescriptor.setFloorId(floorId);

        init();
    }

    /**
     * @see SmartObjectResource
     */
    @Override
    public PeopleSensorDescriptor loadUpdatedValue() {
        return this.peopleSensorDescriptor;
    }

    /**
     * Init Function
     */
    private void init() {
        this.random = new Random(System.currentTimeMillis());

        //this.peopleSensorDescriptor.setPeopleIn(START_PEOPLE + this.random.nextInt(MAX_PEOPLE - START_PEOPLE));
        this.peopleSensorDescriptor.setPeopleIn(0);
        this.peopleSensorDescriptor.setPeopleOut(0);
        this.peopleSensorDescriptor.setCurrentPeople(peopleSensorDescriptor.getPeopleIn());

        start();
    }

    /**
     * Start a new Timer for emulate a People Sensor
     */
    public void start() {
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                peopleSensorDescriptor.setPeopleIn(peopleSensorDescriptor.getPeopleIn() + random.nextInt(START_PEOPLE));
                peopleSensorDescriptor.setPeopleOut(getPeopleOut() + random.nextInt(START_PEOPLE));

                if (peopleSensorDescriptor.getPeopleIn() - peopleSensorDescriptor.getPeopleOut() < MIN_PEOPLE) {
                    peopleSensorDescriptor.setPeopleOut(peopleSensorDescriptor.getPeopleIn());
                }
                else if (peopleSensorDescriptor.getPeopleIn() - peopleSensorDescriptor.getPeopleOut() > MAX_PEOPLE) {
                    peopleSensorDescriptor.setPeopleOut(peopleSensorDescriptor.getPeopleIn() - MAX_PEOPLE);
                }

                peopleSensorDescriptor.setCurrentPeople(peopleSensorDescriptor.getPeopleIn() - peopleSensorDescriptor.getPeopleOut());

                notifyUpdate(peopleSensorDescriptor);
            }
        }, TASK_DELAY_TIME, UPDATE_PERIOD);
    }

    public int getPeopleIn() {
        return peopleSensorDescriptor.getPeopleIn();
    }

    public void setPeopleIn(int peopleIn) {
        this.peopleSensorDescriptor.setPeopleIn(peopleIn);
    }

    public int getPeopleOut() {
        return peopleSensorDescriptor.getPeopleOut();
    }

    public void setPeopleOut(int peopleOut) {
        this.peopleSensorDescriptor.setPeopleOut(peopleOut);
    }

    public int getCurrentPeople() {
        return peopleSensorDescriptor.getCurrentPeople();
    }

    public void setCurrentPeople(int currentPeople) {
        this.peopleSensorDescriptor.setCurrentPeople(currentPeople);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EmulatedPeopleSensor{");
        sb.append("peopleIn=").append(peopleSensorDescriptor.getPeopleIn());
        sb.append(", peopleOut=").append(peopleSensorDescriptor.getPeopleOut());
        sb.append(", currentPeople=").append(peopleSensorDescriptor.getCurrentPeople());
        sb.append('}');
        return sb.toString();
    }
}