package emulated_devices;

import model.ActuatorDescriptor;
import model.DeviceDescriptor;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Emulated Actuator for demo purpose
 */
public class EmulatedActuator extends SmartObjectResource<ActuatorDescriptor> {
    private static final double MAX_CONSUMPTION = 1.0;
    private static final double MIN_CONSUMPTION = 0.2;

    private static final long UPDATE_PERIOD = 5000;
    private static final long TASK_DELAY_TIME = 5000;

    private static final double STARTING_VALUE = 45.0;

    private ActuatorDescriptor actuatorDescriptor;

    private Random random = null;

    private Timer updateTimer = null;

    /**
     * Constructor
     * @param floorId String that represent the floorId of a Floor
     * @param componentId String that represent the componentId of a Device
     * @param deviceId String that represent the unique Id for the Device
     * @param unit The unit of the actuator
     */
    public EmulatedActuator(String floorId, String componentId, String deviceId, String unit) {
        DeviceDescriptor deviceDescriptor = new DeviceDescriptor(floorId, componentId, deviceId, true);
        this.actuatorDescriptor = new ActuatorDescriptor(deviceDescriptor, STARTING_VALUE, unit);

        init();
    }

    /**
     * @see SmartObjectResource
     */
    @Override
    public ActuatorDescriptor loadUpdatedValue() {
        return this.actuatorDescriptor;
    }

    /**
     * Init Function
     */
    private void init() {
        this.random = new Random(System.currentTimeMillis());

        start();
    }

    /**
     * @return +1 or -1
     */
    private int minusOrPlusOne() {
        return Math.random() > 0.5 ? 1 : -1;
    }

    /**
     * Start a new Timer for emulate a sensor
     */
    public void start() {
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (actuatorDescriptor.getDeviceDescriptor().getEnabled()) {
                    actuatorDescriptor.setValue(actuatorDescriptor.getValue() + (MIN_CONSUMPTION + MAX_CONSUMPTION * random.nextDouble()) * minusOrPlusOne());
                    notifyUpdate(actuatorDescriptor);
                }
            }
        }, TASK_DELAY_TIME, UPDATE_PERIOD);
    }

    public ActuatorDescriptor getActuatorDescriptor() {
        return actuatorDescriptor;
    }

    public void setActuatorDescriptor(ActuatorDescriptor actuatorDescriptor) {
        this.actuatorDescriptor = actuatorDescriptor;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EmulatedActuator{");
        sb.append("actuatorDescriptor=").append(actuatorDescriptor.toString());
        sb.append('}');
        return sb.toString();
    }
}