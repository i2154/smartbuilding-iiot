package emulated_devices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class to offer an interface for add, remove and notify generic Listeners, when the resource change
 * @param <T> ActuatorDescriptor or PeopleSensorDescriptor
 */
public abstract class SmartObjectResource<T> {
    private static final Logger logger = LoggerFactory.getLogger(SmartObjectResource.class);

    protected List<ResourceDataListener<T>> resourceDataListeners;

    private String id;
    private String type;

    public SmartObjectResource() {
        this.resourceDataListeners = new ArrayList<>();
    }

    /**
     * Constructor
     * @param id Unique Id to identify a SmartObjectResource
     * @param type Type of the SmartObjectResource
     */
    public SmartObjectResource(String id, String type) {
        this.id = id;
        this.type = type;
        this.resourceDataListeners = new ArrayList<>();
    }

    /**
     * @return Return the updated value of the data
     */
    public abstract T loadUpdatedValue();

    /**
     * Add a Listener in the List of Listeners to notify
     * @param resourceDataListener new Listener
     */
    public void addDataListener(ResourceDataListener<T> resourceDataListener) {
        if (this.resourceDataListeners != null) {
            this.resourceDataListeners.add(resourceDataListener);
        }
    }

    /**
     * Remove a listener in the List of Listeners to notify
     * @param resourceDataListener Listener
     */
    public void removeDataListener(ResourceDataListener<T> resourceDataListener) {
        if (this.resourceDataListeners != null && this.resourceDataListeners.contains(resourceDataListener)) {
            this.resourceDataListeners.remove(resourceDataListener);
        }
    }

    /**
     * Notify the updated Value to all registered Listeners in the Listeners List
     * @param updatedValue Updated value
     */
    public void notifyUpdate(T updatedValue) {
        if (this.resourceDataListeners != null && this.resourceDataListeners.size() > 0) {
            this.resourceDataListeners.forEach(resourceDataListener -> {
                if (resourceDataListener != null) {
                    resourceDataListener.onDataChanged(this, updatedValue);
                }
            });
        }
        else {
            logger.error("Empty or Null Resource Data Listener List! Nothing to notify...");
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SmartObjectResource{");
        sb.append("id='").append(id).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append('}');
        return sb.toString();
    }
}