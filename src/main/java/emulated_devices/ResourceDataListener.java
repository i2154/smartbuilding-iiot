package emulated_devices;

/**
 * Interface for
 * @param <T> ActuatorDescriptor or PeopleSensorDescriptor
 */
public interface ResourceDataListener<T> {
    /**
     *
     * @param resource Resource changed to notify to the listeners
     * @param updatedValue New Value updated
     */
    public void onDataChanged(SmartObjectResource<T> resource, T updatedValue);
}