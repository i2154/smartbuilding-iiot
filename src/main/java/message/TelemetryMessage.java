package message;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Message used for telemetry reasons (Actuator and People Sensor)
 * @param <T> This param is ActuatorDescriptor or PeopleSensorDescriptor, used for generic reasons
 */
public class TelemetryMessage<T> extends GenericMessage {

    @JsonProperty("dataValue")
    private T dataValue;

    public TelemetryMessage() {}

    /**
     * Constructor
     * @param type The type of the message
     * @param timestamp Timestamp when the message is created
     * @param dataValue Telemetry Value (ActuatorDescriptor or PeopleSensorDescriptor)
     */
    public TelemetryMessage(String type, long timestamp, T dataValue) {
        super(type, timestamp);
        this.dataValue = dataValue;
    }

    public T getDataValue() {
        return dataValue;
    }

    public void setDataValue(T dataValue) {
        this.dataValue = dataValue;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TelemetryMessage{");
        sb.append("dataValue=").append(dataValue.toString());
        sb.append('}');
        return sb.toString();
    }
}