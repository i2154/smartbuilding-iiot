package message;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Generic message
 */
public abstract class GenericMessage {
    @JsonProperty("type")
    private String type;

    @JsonProperty("timestamp")
    private long timestamp;

    public GenericMessage() {}

    /**
     * Constructor
     * @param type The type of the message
     * @param timestamp Timestamp when the message is created
     */
    public GenericMessage(String type, long timestamp) {
        this.type = type;
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GenericMessage{");
        sb.append("type='").append(type).append('\'');
        sb.append(", timestamp=").append(timestamp);
        sb.append('}');
        return sb.toString();
    }
}