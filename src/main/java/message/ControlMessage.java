package message;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Message to control the Actuators
 */
public class ControlMessage extends GenericMessage{
    @JsonProperty("isActive")
    private Boolean isActive;

    public ControlMessage() {}

    public ControlMessage(long timestamp, Boolean isActive) {
        super("iot:snbld:control", timestamp);
        this.isActive = isActive;
    }

    /**
     * Constructor
     * @param type The type of the message
     * @param timestamp Timestamp when the message is created
     * @param isActive On (True) or Off (False) the actuator
     */
    public ControlMessage(String type, long timestamp, Boolean isActive) {
        super(type, timestamp);
        this.isActive = isActive;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ControlMessage{");
        sb.append("change=").append(isActive);
        sb.append('}');
        return sb.toString();
    }
}