package server;

import client.ClientHelper;
import client.MqttHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.ActuatorDescriptor;
import model.PeopleSensorDescriptor;
import model.PolicyDescriptor;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

import io.dropwizard.jersey.errors.ErrorMessage;

@Path("/floor")
public class HttpServer {
    private static final Logger logger = LoggerFactory.getLogger(HttpServer.class);

    private final ClientHelper clientHelper;
    private final MqttHelper mqttHelper;
    private final ObjectMapper objectMapper;

    @Inject
    public HttpServer(ClientHelper clientHelper, MqttHelper mqttHelper) {
        this.clientHelper = clientHelper;
        this.mqttHelper = mqttHelper;
        this.objectMapper = new ObjectMapper();
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllPeopleSensors(@Context ContainerRequestContext req) {
        ArrayList<String> floors = new ArrayList<>(clientHelper.getPeopleSensorMap().values().stream().map(PeopleSensorDescriptor::getFloorId).toList());

        if (floors.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND)
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(), "People sensors not found"))
                    .build();
        }

        try {
            return Response.ok(this.objectMapper.writeValueAsString(floors)).build();
        } catch (JsonProcessingException e) {
            return makeInternalServerErrorResponse(e);
        }
    }

    @GET
    @Path("/{floor-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPeopleSensorInformation(@Context ContainerRequestContext req, @PathParam("floor-id") String floorId) {
        if(this.clientHelper.getPeopleSensorMap().containsKey(floorId)) {
            try {
                return Response.ok(this.objectMapper.writeValueAsString(this.clientHelper.getPeopleSensorMap().get(floorId))).build();
            } catch (JsonProcessingException e) {
                return makeInternalServerErrorResponse(e);
            }
        }
        else {
            return Response.status(Response.Status.NOT_FOUND)
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(), "sensor: " + floorId + " not found!"))
                    .build();
        }
    }

    @GET
    @Path("/{floor-id}/component")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllComponentsOnFloor(@Context ContainerRequestContext req, @PathParam("floor-id") String floorId) {
        if(this.clientHelper.getPeopleSensorMap().containsKey(floorId) && this.clientHelper.getDevicesMap().keySet().stream().map((ImmutableTriple::getLeft)).anyMatch(s -> s.equals(floorId))) {
            try {
                return Response.ok(this.objectMapper.writeValueAsString(this.clientHelper.getDevicesMap()
                                                                                        .keySet()
                                                                                        .stream()
                                                                                        .filter(stringStringStringImmutableTriple -> stringStringStringImmutableTriple.getLeft().equals(floorId))
                                                                                        .map(ImmutableTriple::getMiddle)
                                                                                        .distinct()
                                                                                        .toList()))
                                                                                        .build();
            } catch (JsonProcessingException e) {
                return makeInternalServerErrorResponse(e);
            }
        }
        else {
            return Response.status(Response.Status.NOT_FOUND)
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(), "components on floor : " + floorId + " not found!"))
                    .build();
        }
    }

    @GET
    @Path("/{floor-id}/component/{component-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDevicesAssociatedToComponent(@Context ContainerRequestContext req, @PathParam("floor-id") String floorId, @PathParam("component-id") String componentId) {
        if(this.clientHelper.getPeopleSensorMap().containsKey(floorId)
                && this.clientHelper.getDevicesMap().keySet().stream().map(ImmutableTriple::getLeft).anyMatch(s -> s.equals(floorId))
                && this.clientHelper.getDevicesMap().keySet().stream().map(ImmutableTriple::getMiddle).anyMatch(s -> s.equals(componentId))) {

            ArrayList<String> devices = new ArrayList<>(this.clientHelper.getDevicesMap()
                    .keySet()
                    .stream()
                    .filter(stringStringStringImmutableTriple -> stringStringStringImmutableTriple.getLeft().equals(floorId) && stringStringStringImmutableTriple.getMiddle().equals(componentId))
                    .map(ImmutableTriple::getRight)
                    .toList());

            try {
                return Response.ok(this.objectMapper.writeValueAsString(devices)).build();
            } catch (JsonProcessingException e) {
                return makeInternalServerErrorResponse(e);
            }
        }
        else {
            return Response.status(Response.Status.NOT_FOUND)
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(),"FloorId or ComponentId NOT FOUND"))
                    .build();
        }
    }

    @GET
    @Path("/{floor-id}/component/{component-id}/{type}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDeviceInfo(@Context ContainerRequestContext req, @PathParam("floor-id") String floorId, @PathParam("component-id") String componentId, @PathParam("type") String type) {
        if(this.clientHelper.getPeopleSensorMap().containsKey(floorId)
                && this.clientHelper.getDevicesMap().keySet().stream().map(ImmutableTriple::getLeft).anyMatch(s -> s.equals(floorId))
                && this.clientHelper.getDevicesMap().keySet().stream().map(ImmutableTriple::getMiddle).anyMatch(s -> s.equals(componentId))) {

            ActuatorDescriptor requested = this.clientHelper.getDevicesMap().get(new ImmutableTriple<>(floorId, componentId, type));

            if(requested != null) {
                try {
                    return Response.ok(this.objectMapper.writeValueAsString(requested)).build();
                } catch (JsonProcessingException e) {
                    return makeInternalServerErrorResponse(e);
                }
            }
        }

        return Response.status(Response.Status.NOT_FOUND)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(),"In floor:" + floorId + " and component: " + componentId + "NOT FOUND type:" + type))
                .build();
    }

    @GET
    @Path("/{floor-id}/component/{component-id}/{type}/policy")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDevicePolicy(@Context ContainerRequestContext req, @PathParam("floor-id") String floorId, @PathParam("component-id") String componentId, @PathParam("type") String type) {
        if(this.clientHelper.getPeopleSensorMap().containsKey(floorId)
                && this.clientHelper.getDevicesMap().keySet().stream().map(ImmutableTriple::getLeft).anyMatch(s -> s.equals(floorId))
                && this.clientHelper.getDevicesMap().keySet().stream().map(ImmutableTriple::getMiddle).anyMatch(s -> s.equals(componentId))) {

            PolicyDescriptor requested = this.clientHelper.getPoliciesHashMap().get(new ImmutableTriple<>(floorId, componentId, type));

            if(requested != null) {
                try {
                    return Response.ok(this.objectMapper.writeValueAsString(requested)).build();
                } catch (JsonProcessingException e) {
                    return makeInternalServerErrorResponse(e);
                }
            }
        }

        return Response.status(Response.Status.NOT_FOUND)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(),"In floor:" + floorId + " and component: " + componentId + " and type" + type + " NOT FOUND POLICY"))
                .build();
    }

    @POST
    @Path("/{floor-id}/component/{component-id}/{type}/policy")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setDevicePolicy(@Context ContainerRequestContext req, @PathParam("floor-id") String floorId,
                                    @PathParam("component-id") String componentId, @PathParam("type") String type, PolicyDescriptor policyDescriptor) {
        if(this.clientHelper.getPeopleSensorMap().containsKey(floorId)) {
            if (this.clientHelper.getPoliciesHashMap().get(new ImmutableTriple<>(floorId, componentId, type)) == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .type(MediaType.APPLICATION_JSON_TYPE)
                        .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(), "Not found the device: " + floorId + componentId + type))
                        .build();
            }
            else {
                this.clientHelper.getPoliciesHashMap().get(new ImmutableTriple<>(floorId, componentId, type))
                        .setEnabled(policyDescriptor.getEnabled());
                this.clientHelper.getPoliciesHashMap().get(new ImmutableTriple<>(floorId, componentId, type))
                        .setTimeToReactivate(policyDescriptor.getTimeToReactivate());
                this.clientHelper.getPoliciesHashMap().get(new ImmutableTriple<>(floorId, componentId, type))
                        .setMaxConsume(policyDescriptor.getMaxConsume());

                return Response.noContent().build();
            }
        }
        else {
            return Response.status(Response.Status.NOT_FOUND)
                    .type(MediaType.APPLICATION_JSON_TYPE)
                    .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(), "Not found the device: " + floorId + componentId + type))
                    .build();
        }
    }

    private Response makeInternalServerErrorResponse (JsonProcessingException e) {
        logger.error("Cannot construct response. Msg: {}", e.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(new ErrorMessage(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),"Internal Server Error !"))
                .build();
    }
}