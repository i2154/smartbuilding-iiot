module smartbuilding.IoT {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires org.slf4j;
    requires org.eclipse.paho.client.mqttv3;
    requires com.opencsv;
    requires org.apache.commons.lang3;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires java.ws.rs;
    requires javax.inject;
    requires dropwizard.jersey;
    requires dropwizard.core;
    requires java.xml;

    exports client;
    exports devices;
    exports emulated_devices;
    exports gui;
    exports message;
    exports model;
    exports server;
}
