package gui;

/**
 * Class useful only to compile to jar with Maven for a Bug
 */
public class GuiStarter {
    public static void main(String[] args) {
        SmartbuildingGui.main(args);
    }
}