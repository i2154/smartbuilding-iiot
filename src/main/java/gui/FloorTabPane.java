package gui;

import client.ClientHelper;
import javafx.scene.control.TabPane;
import model.ActuatorDescriptor;
import model.PeopleSensorDescriptor;
import model.PolicyDescriptor;
import org.apache.commons.lang3.tuple.ImmutableTriple;

import java.util.HashMap;
import java.util.Map;

/**
 * The Panel to manage various Tabs, assigned to every Floor
 */
public class FloorTabPane extends TabPane {
    private final int SCREEN_WIDTH, SCREEN_HEIGHT;

    private ClientHelper clientHelper;

    HashMap<String, FloorTab> floorTabs;

    /**
     * Constructor
     * @param SCREEN_WIDTH Screen width
     * @param SCREEN_HEIGHT Screen height
     * @param clientHelper Client helper for interface with the Client
     */
    public FloorTabPane(int SCREEN_WIDTH, int SCREEN_HEIGHT, ClientHelper clientHelper) {
        super();

        this.SCREEN_HEIGHT = SCREEN_HEIGHT;
        this.SCREEN_WIDTH = SCREEN_WIDTH;

        this.clientHelper = clientHelper;

        this.floorTabs = new HashMap<>();
    }

    /**
     * Check if a floor is present, if not add to the HashMap of FloorTab, if yes update
     *
     * @param devicesMap Map of all devices (Actuators)
     * @param peopleSensorMap Map of all people sensors
     * @param policiesHashMap Map of all Policies attached to Actuators
     */
    public void updateTabs(Map<ImmutableTriple<String, String, String>, ActuatorDescriptor> devicesMap,
                           Map<String, PeopleSensorDescriptor> peopleSensorMap,
                           HashMap<ImmutableTriple<String, String, String>, PolicyDescriptor> policiesHashMap) {

        peopleSensorMap.forEach((floorId, peopleSensorDescriptor) -> {
            if (!this.floorTabs.containsKey(floorId)) {
                this.floorTabs.put(floorId, new FloorTab(floorId, SCREEN_WIDTH, SCREEN_HEIGHT, clientHelper));
                this.getTabs().add(this.floorTabs.get(floorId));
                this.floorTabs.get(floorId).update(devicesMap, peopleSensorMap, policiesHashMap);
            } else {
                this.floorTabs.get(floorId).update(devicesMap, peopleSensorMap, policiesHashMap);
            }
        });
    }
}