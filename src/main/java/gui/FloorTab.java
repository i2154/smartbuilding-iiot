package gui;

import client.ClientHelper;
import client.MqttHelper;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import model.ActuatorDescriptor;
import model.PeopleSensorDescriptor;
import model.PolicyDescriptor;
import org.apache.commons.lang3.tuple.ImmutableTriple;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that represent a custom Tab to manage all devices on the specified Floor and attached to various component
 */
public class FloorTab extends Tab {
    private final int SCREEN_WIDTH, SCREEN_HEIGHT;

    private double gasPrice, waterPrice, elecPrice;

    private final HashMap<ImmutableTriple<String, String, String>, Double> consumptionValues;

    private final ClientHelper clientHelper;

    HashMap<String, HashMap<String, Double>> meanValues;

    private VBox actuatorsVbox;
    private VBox waterVbox;
    private VBox gasVbox;
    private VBox electricityVbox;

    private HBox peopleSensorHbox;
    private final HBox waterTotalHbox;
    private final HBox gasTotalHbox;
    private final HBox electricityTotalHbox;
    private HBox componentChoiceBoxHbox;

    private Label currentPeople, peopleIn, peopleOut, currentPeopleFlavourText, peopleInFlavourText, peopleOutFlavourText;
    private Label gasActuatorValue;
    private Label gasIsActiveValue;
    private Label waterActuatorValue;
    private Label waterIsActiveValue;
    private Label electricityActuatorValue;
    private Label electricityIsActiveValue;
    private Label gasConsumptionValue;
    private Label electricityConsumptionValue;
    private Label waterConsumptionValue;
    private Label waterMeanValue;
    private Label gasMeanValue;
    private Label electricityMeanValue;

    private final ImageView gasImageView;
    private final ImageView waterImageView;
    private final ImageView electricityImageView;

    private CheckBox waterPolicyCheckBox, gasPolicyCheckBox, electricityPolicyCheckBox;

    private TextField timeToReactivateWaterTF, timeToReactivateGasTF, timeToReactivateElectricityTF;
    private TextField gasPriceTextField, waterPriceTextField, elecPriceTextField;

    private ChoiceBox<String> componentChoiceBox;
    private String selectedComponent = null;

    private HashMap<ImmutableTriple<String, String, String>, PolicyDescriptor> policiesHashMap = null;

    /**
     * Constructor
     *
     * @param s String that represent the FloorId
     * @param SCREEN_WIDTH Screen Width
     * @param SCREEN_HEIGHT Screen Height
     * @param clientHelper Client Helper to interface with
     */
    public FloorTab(String s, int SCREEN_WIDTH, int SCREEN_HEIGHT, ClientHelper clientHelper) {
        super(s);

        this.SCREEN_HEIGHT = SCREEN_HEIGHT;
        this.SCREEN_WIDTH = SCREEN_WIDTH;

        this.consumptionValues = new HashMap<>();
        this.meanValues = new HashMap<>();

        this.clientHelper = clientHelper;

        VBox root = new VBox();

        this.waterTotalHbox = new HBox();
        this.gasTotalHbox = new HBox();
        this.electricityTotalHbox = new HBox();

        HBox chartHbox = new HBox();
        chartHbox.setPadding(new Insets(10));
        chartHbox.setAlignment(Pos.CENTER);

        this.electricityImageView = new ImageView();
        this.gasImageView = new ImageView();
        this.waterImageView = new ImageView();

        gasImageView.setFitHeight(360);
        gasImageView.setFitWidth(480);

        waterImageView.setFitHeight(360);
        waterImageView.setFitWidth(480);

        electricityImageView.setFitHeight(360);
        electricityImageView.setFitWidth(480);

        chartHbox.getChildren().addAll(gasImageView, waterImageView, electricityImageView);

        initializeChoiceBox();
        initializeVboxActuators();
        initializeTextLabelPeopleSensor();
        initializePeopleSensorHBox();
        initializeTextLabelActuators();
        initializeMeanValueLabels();
        initializePolicyCheckBox();
        initializePolicyTextFieldAndButtons();
        initializeCalculateButtonAndTextField();

        root.getChildren().add(peopleSensorHbox);
        root.getChildren().add(componentChoiceBoxHbox);
        root.getChildren().add(actuatorsVbox);
        root.getChildren().add(chartHbox);

        root.setAlignment(Pos.CENTER);

        this.setContent(root);
    }

    /**
     * Updates all Components of the GUI (Tab)
     *
     * @param devicesMap Map of all devices (Actuators)
     * @param peopleSensorMap Map of all people sensors
     * @param policiesHashMap Map of all Policies attached to Actuators
     */
    protected void update(Map<ImmutableTriple<String, String, String>, ActuatorDescriptor> devicesMap,
                          Map<String, PeopleSensorDescriptor> peopleSensorMap,
                          HashMap<ImmutableTriple<String, String, String>, PolicyDescriptor> policiesHashMap) {

        this.policiesHashMap = policiesHashMap;

        populatePeopleSensorInfo(peopleSensorMap);
        populateComponentChoiceBox(devicesMap);
        populateDevicesInfo(devicesMap);
        populatePolicyCheckBox(policiesHashMap);
        populateAndUpdateConsumption(devicesMap);
        updateMeanValues();
        displayChart();
    }

    /**
     * Update the mean value Labels from the mean values hashmap
     */
    private void updateMeanValues() {
        if (this.meanValues.containsKey(this.selectedComponent)) {
            this.gasMeanValue.setText(String.valueOf(this.meanValues.get(this.selectedComponent).get("iot:snbld:gas-actuator")).concat(" \u20ac"));
            this.waterMeanValue.setText(String.valueOf(this.meanValues.get(this.selectedComponent).get("iot:snbld:water-actuator")).concat(" \u20ac"));
            this.electricityMeanValue.setText(String.valueOf(this.meanValues.get(this.selectedComponent).get("iot:snbld:electricity-actuator")).concat(" \u20ac"));
        }
    }

    /**
     * Update the Consumption Labels if they are present (After a calculation of 60 seconds)
     *
     * @param devicesMap Map of all devices (Actuators)
     */
    private void populateAndUpdateConsumption(Map<ImmutableTriple<String, String, String>, ActuatorDescriptor> devicesMap) {
        devicesMap.keySet().forEach(stringStringImmutableTriple -> {
            if (devicesMap.get(stringStringImmutableTriple).getDeviceDescriptor().getEnabled()) {
                if (this.consumptionValues.containsKey(stringStringImmutableTriple)) {
                    this.consumptionValues.put(stringStringImmutableTriple, consumptionValues.get(stringStringImmutableTriple) + devicesMap.get(stringStringImmutableTriple).getValue());
                } else {
                    this.consumptionValues.put(stringStringImmutableTriple, devicesMap.get(stringStringImmutableTriple).getValue());
                }
            }
        });
        try {
            this.gasConsumptionValue.setText(this.consumptionValues.get(new ImmutableTriple<>(this.getText(), this.selectedComponent, "iot:snbld:gas-actuator")).toString().concat(" kg"));
        } catch (NullPointerException e) {
            this.gasConsumptionValue.setText("0");
        }

        try {
            this.electricityConsumptionValue.setText(this.consumptionValues.get(new ImmutableTriple<>(this.getText(), this.selectedComponent, "iot:snbld:electricity-actuator")).toString().concat(" kWh"));
        } catch (NullPointerException e) {
            this.electricityConsumptionValue.setText("0");
        }

        try {
            this.waterConsumptionValue.setText(this.consumptionValues.get(new ImmutableTriple<>(this.getText(), this.selectedComponent, "iot:snbld:water-actuator")).toString().concat(" l/s"));
        } catch (NullPointerException e) {
            this.waterConsumptionValue.setText("0");
        }
    }

    /**
     * Display the chart after the calculation
     */
    public void displayChart() {
        gasImageView.setImage(new Image(String.format("file:/home/rhohen/Workspace/smartbuilding-iot/src/main/resources/%s%sgas.png", this.getText(), this.selectedComponent)));
        electricityImageView.setImage(new Image(String.format("file:/home/rhohen/Workspace/smartbuilding-iot/src/main/resources/%s%selectricity.png", this.getText(), this.selectedComponent)));
        waterImageView.setImage(new Image(String.format("file:/home/rhohen/Workspace/smartbuilding-iot/src/main/resources/%s%swater.png", this.getText(), this.selectedComponent)));
    }

    /**
     * Update the check box for activate or deactivate Policy for a specific actuator
     *
     * @param policiesHashMap Map of all Policies attached to Actuators
     */
    private void populatePolicyCheckBox(HashMap<ImmutableTriple<String, String, String>, PolicyDescriptor> policiesHashMap) {
        policiesHashMap.keySet()
                .stream()
                .filter(stringStringImmutableTriple -> stringStringImmutableTriple.left.equals(this.getText()) && stringStringImmutableTriple.middle.equals(this.selectedComponent))
                .forEach(stringStringImmutableTriple -> {
                    switch (stringStringImmutableTriple.getRight()) {
                        case "iot:snbld:electricity-actuator" -> this.electricityPolicyCheckBox.setSelected(policiesHashMap.get(new ImmutableTriple<>(this.getText(), stringStringImmutableTriple.getMiddle(), stringStringImmutableTriple.getRight())).getEnabled());
                        case "iot:snbld:gas-actuator" -> this.gasPolicyCheckBox.setSelected(policiesHashMap.get(new ImmutableTriple<>(this.getText(), stringStringImmutableTriple.getMiddle(), stringStringImmutableTriple.getRight())).getEnabled());
                        case "iot:snbld:water-actuator" -> this.waterPolicyCheckBox.setSelected(policiesHashMap.get(new ImmutableTriple<>(this.getText(), stringStringImmutableTriple.getMiddle(), stringStringImmutableTriple.getRight())).getEnabled());
                    }
                });
    }

    /**
     * Update every device info (Actuator)
     *
     * @param devicesMap Map of all devices (Actuators)
     */
    private void populateDevicesInfo(Map<ImmutableTriple<String, String, String>, ActuatorDescriptor> devicesMap) {

        if (this.selectedComponent != null) {
            waterTotalHbox.setVisible(false);
            gasTotalHbox.setVisible(false);
            electricityTotalHbox.setVisible(false);

            devicesMap
                    .values()
                    .stream()
                    .filter(actuatorDescriptor -> actuatorDescriptor.getDeviceDescriptor().getFloorId().equals(this.getText())
                            && actuatorDescriptor.getDeviceDescriptor().getComponentId().equals(this.selectedComponent))
                    .forEach(this::constructDeviceInfo);
        }
    }

    /**
     * Update a specific actuator infos
     *
     * @param actuatorDescriptor Actuator Descriptor to update
     */
    private void constructDeviceInfo(ActuatorDescriptor actuatorDescriptor) {
        switch (MqttHelper.unitToActuatorType(actuatorDescriptor.getUnit())) {
            case "water" -> {
                waterIsActiveValue.setText(String.valueOf(actuatorDescriptor.getDeviceDescriptor().getEnabled()));
                waterActuatorValue.setText(String.valueOf(actuatorDescriptor.getValue()).concat(" ").concat(actuatorDescriptor.getUnit()));
                waterTotalHbox.setVisible(true);
            }
            case "gas" -> {
                gasIsActiveValue.setText(String.valueOf(actuatorDescriptor.getDeviceDescriptor().getEnabled()));
                gasActuatorValue.setText(String.valueOf(actuatorDescriptor.getValue()).concat(" ").concat(actuatorDescriptor.getUnit()));
                gasTotalHbox.setVisible(true);
            }
            case "electricity" -> {
                electricityIsActiveValue.setText(String.valueOf(actuatorDescriptor.getDeviceDescriptor().getEnabled()));
                electricityActuatorValue.setText(String.valueOf(actuatorDescriptor.getValue()).concat(" ").concat(actuatorDescriptor.getUnit()));
                electricityTotalHbox.setVisible(true);
            }
        }
    }

    /**
     * Update the people sensor Labels
     *
     * @param peopleSensorMap Map of all people sensors
     */
    private void populatePeopleSensorInfo(Map<String, PeopleSensorDescriptor> peopleSensorMap) {
        this.currentPeople.setText(String.valueOf(peopleSensorMap.get(this.getText()).getCurrentPeople()));
        this.peopleIn.setText(String.valueOf(peopleSensorMap.get(this.getText()).getPeopleIn()));
        this.peopleOut.setText(String.valueOf(peopleSensorMap.get(this.getText()).getPeopleOut()));
    }

    /**
     * Update the Choice Box that list every component that has Devices
     *
     * @param devicesMap Map of all devices (Actuators)
     */
    private void populateComponentChoiceBox(Map<ImmutableTriple<String, String, String>, ActuatorDescriptor> devicesMap) {
        this.componentChoiceBox.getItems()
                .addAll(devicesMap.keySet()
                        .stream()
                        .filter(stringStringImmutableTriple -> stringStringImmutableTriple.left.equals(this.getText()))
                        .map(ImmutableTriple::getMiddle)
                        .takeWhile(s -> !this.componentChoiceBox.getItems().contains(s))
                        .distinct()
                        .toList());

    }

    /**
     * Initialize the mean value labels
     */
    private void initializeMeanValueLabels() {
        HBox meanValueGasHbox = new HBox();
        meanValueGasHbox.setPadding(new Insets(20));

        HBox meanValueWaterHbox = new HBox();
        meanValueWaterHbox.setPadding(new Insets(20));

        HBox meanValueElecHbox = new HBox();
        meanValueElecHbox.setPadding(new Insets(20));

        Label waterMeanValueFlavourText = new Label("Water mean cost (per second): ");
        Label gasMeanValueFlavourText = new Label("Gas mean cost (per second): ");
        Label electricityMeanValueFlavourText = new Label("Electricity mean cost (per second): ");

        this.waterMeanValue = new Label();
        this.gasMeanValue = new Label();
        this.electricityMeanValue = new Label();

        meanValueWaterHbox.getChildren().addAll(waterMeanValueFlavourText, waterMeanValue);
        meanValueGasHbox.getChildren().addAll(gasMeanValueFlavourText, gasMeanValue);
        meanValueElecHbox.getChildren().addAll(electricityMeanValueFlavourText, electricityMeanValue);

        waterTotalHbox.getChildren().add(meanValueWaterHbox);
        gasTotalHbox.getChildren().add(meanValueGasHbox);
        electricityTotalHbox.getChildren().add(meanValueElecHbox);
    }

    /**
     * Initialize the Calculation button and TextField
     */
    private void initializeCalculateButtonAndTextField() {
        HBox calculateButtonHbox = new HBox();
        calculateButtonHbox.setPadding(new Insets(20));

        this.gasPriceTextField = new TextField();
        gasPriceTextField.setPromptText("gasPrice");
        this.waterPriceTextField = new TextField();
        waterPriceTextField.setPromptText("waterPrice");
        this.elecPriceTextField = new TextField();
        elecPriceTextField.setPromptText("elecPrice");

        calculateButtonHbox.getChildren().addAll(gasPriceTextField, waterPriceTextField, elecPriceTextField);

        Button calculateStatsButton = new Button("Calculate (60 seconds)");
        calculateButtonHbox.getChildren().add(calculateStatsButton);

        calculateStatsButton.setOnAction(actionEvent -> {
            try {
                if (!this.elecPriceTextField.getText().isEmpty() || Double.parseDouble(this.elecPriceTextField.getText()) != 0) {
                    this.elecPrice = Double.parseDouble(this.elecPriceTextField.getText());
                }
            } catch (NumberFormatException e) {
                this.elecPrice = clientHelper.getElectricityPrice();
            }

            try {
                if (!this.gasPriceTextField.getText().isEmpty() || Double.parseDouble(this.gasPriceTextField.getText()) != 0) {
                    this.gasPrice = Double.parseDouble(this.gasPriceTextField.getText());
                }
            } catch (NumberFormatException e) {
                this.gasPrice = clientHelper.getGasPrice();
            }

            try {
                if (!this.waterPriceTextField.getText().isEmpty() || Double.parseDouble(this.waterPriceTextField.getText()) != 0) {
                    this.waterPrice = Double.parseDouble(this.waterPriceTextField.getText());
                }
            } catch (NumberFormatException e) {
                this.waterPrice = clientHelper.getWaterPrice();
            }

            new Thread(() -> {
                try {
                    this.meanValues.put(this.selectedComponent, clientHelper.calculateMeanValue(this.getText(), this.selectedComponent, this.gasPrice, this.waterPrice, this.elecPrice));
                } catch (InterruptedException e) {
                    System.err.println("Cannot sleep" + e.getMessage());
                }
            }).start();

            new Thread(() -> {
                try {
                    clientHelper.writeActuatorDataToCsvFile(this);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        });

        this.actuatorsVbox.getChildren().add(calculateButtonHbox);
    }

    /**
     * Initialize the Policy text field and buttons
     */
    private void initializePolicyTextFieldAndButtons() {
        HBox waterPolicyHbox = new HBox();
        HBox gasPolicyHbox = new HBox();
        HBox electricityPolicyHbox = new HBox();

        waterPolicyHbox.setPadding(new Insets(20));
        gasPolicyHbox.setPadding(new Insets(20));
        electricityPolicyHbox.setPadding(new Insets(20));

        this.timeToReactivateElectricityTF = new TextField();
        this.timeToReactivateWaterTF = new TextField();
        this.timeToReactivateGasTF = new TextField();

        Button electricityPolicyButton = new Button("Set Policy");
        Button gasPolicyButton = new Button("Set Policy");
        Button waterPolicyButton = new Button("Set Policy");

        electricityPolicyButton.setOnAction(actionEvent -> {
            int tmp = Integer.parseInt(timeToReactivateElectricityTF.getText());
            if (tmp > 1) {
                this.policiesHashMap.get(new ImmutableTriple<>(this.getText(), selectedComponent, "iot:snbld:electricity-actuator")).setTimeToReactivate(tmp);
            }
        });

        waterPolicyButton.setOnAction(actionEvent -> {
            int tmp = Integer.parseInt(timeToReactivateWaterTF.getText());
            if (tmp > 1) {
                this.policiesHashMap.get(new ImmutableTriple<>(this.getText(), selectedComponent, "iot:snbld:water-actuator")).setTimeToReactivate(tmp);
            }
        });

        gasPolicyButton.setOnAction(actionEvent -> {
            int tmp = Integer.parseInt(timeToReactivateGasTF.getText());
            if (tmp > 1) {
                this.policiesHashMap.get(new ImmutableTriple<>(this.getText(), selectedComponent, "iot:snbld:gas-actuator")).setTimeToReactivate(tmp);
            }
        });

        electricityPolicyHbox.getChildren().addAll(timeToReactivateElectricityTF, electricityPolicyButton);
        gasPolicyHbox.getChildren().addAll(timeToReactivateGasTF, gasPolicyButton);
        waterPolicyHbox.getChildren().addAll(timeToReactivateWaterTF, waterPolicyButton);

        this.waterTotalHbox.getChildren().add(waterPolicyHbox);
        this.gasTotalHbox.getChildren().add(gasPolicyHbox);
        this.electricityTotalHbox.getChildren().add(electricityPolicyHbox);

    }

    /**
     * Initialize the check boxes of policies
     */
    private void initializePolicyCheckBox() {
        this.waterPolicyCheckBox = new CheckBox("Water Actuator Policy");
        this.gasPolicyCheckBox = new CheckBox("Gas Actuator Policy");
        this.electricityPolicyCheckBox = new CheckBox("Electricity Actuator Policy");
        this.waterPolicyCheckBox.setPadding(new Insets(20));
        this.gasPolicyCheckBox.setPadding(new Insets(20));
        this.electricityPolicyCheckBox.setPadding(new Insets(20));

        waterPolicyCheckBox.selectedProperty().addListener((observableValue, aBoolean, t1) -> this.policiesHashMap.get(new ImmutableTriple<>(this.getText(), this.selectedComponent, "iot:snbld:water-actuator")).setEnabled(t1));

        gasPolicyCheckBox.selectedProperty().addListener((observableValue, aBoolean, t1) -> this.policiesHashMap.get(new ImmutableTriple<>(this.getText(), this.selectedComponent, "iot:snbld:gas-actuator")).setEnabled(t1));

        electricityPolicyCheckBox.selectedProperty().addListener((observableValue, aBoolean, t1) -> this.policiesHashMap.get(new ImmutableTriple<>(this.getText(), this.selectedComponent, "iot:snbld:electricity-actuator")).setEnabled(t1));

        gasTotalHbox.getChildren().add(gasPolicyCheckBox);
        waterTotalHbox.getChildren().add(waterPolicyCheckBox);
        electricityTotalHbox.getChildren().add(electricityPolicyCheckBox);
    }

    /**
     * Initialize the choice box of components
     */
    private void initializeChoiceBox() {
        componentChoiceBoxHbox = new HBox();
        componentChoiceBoxHbox.setPadding(new Insets(20));

        this.componentChoiceBox = new ChoiceBox<>();
        componentChoiceBox.getSelectionModel().selectedItemProperty().addListener((observableValue, s1, t1) -> this.selectedComponent = t1);
        this.componentChoiceBox.setPrefWidth((double) SCREEN_HEIGHT / 4);

        componentChoiceBoxHbox.getChildren().add(componentChoiceBox);

    }

    /**
     * Initialize the people sensor text labels
     */
    private void initializeTextLabelPeopleSensor() {
        currentPeopleFlavourText = new Label("Current People: ");
        currentPeopleFlavourText.setPrefWidth((double) SCREEN_WIDTH / 6);
        currentPeopleFlavourText.setAlignment(Pos.CENTER);

        currentPeople = new Label();
        currentPeople.setPrefWidth((double) SCREEN_WIDTH / 6);

        peopleInFlavourText = new Label("People In: ");
        peopleInFlavourText.setPrefWidth((double) SCREEN_WIDTH / 6);
        peopleInFlavourText.setAlignment(Pos.CENTER);

        peopleIn = new Label();
        peopleIn.setPrefWidth((double) SCREEN_WIDTH / 6);

        peopleOutFlavourText = new Label("People Out: ");
        peopleOutFlavourText.setPrefWidth((double) SCREEN_WIDTH / 6);
        peopleOutFlavourText.setAlignment(Pos.CENTER);

        peopleOut = new Label();
        peopleOut.setPrefWidth((double) SCREEN_WIDTH / 6);
    }

    /**
     * Initialize the people sensor horizontal box
     */
    private void initializePeopleSensorHBox() {
        this.peopleSensorHbox = new HBox();
        peopleSensorHbox.setPadding(new Insets(20));

        peopleSensorHbox.getChildren().add(currentPeopleFlavourText);
        peopleSensorHbox.getChildren().add(this.currentPeople);
        peopleSensorHbox.getChildren().add(peopleInFlavourText);
        peopleSensorHbox.getChildren().add(this.peopleIn);
        peopleSensorHbox.getChildren().add(peopleOutFlavourText);
        peopleSensorHbox.getChildren().add(this.peopleOut);
    }

    /**
     * Initialize the vertical boxes
     */
    private void initializeVboxActuators() {
        this.actuatorsVbox = new VBox();

        this.waterVbox = new VBox();
        waterVbox.setPadding(new Insets(20));

        this.gasVbox = new VBox();
        gasVbox.setPadding(new Insets(20));

        this.electricityVbox = new VBox();
        electricityVbox.setPadding(new Insets(20));
    }

    /**
     * Initialize all text labels and fields of Actuators infos
     */
    private void initializeTextLabelActuators() {
        HBox waterIsActiveHbox = new HBox();
        HBox waterValueHbox = new HBox();

        HBox gasIsActiveHbox = new HBox();
        HBox gasValueHbox = new HBox();

        HBox electricityIsActiveHbox = new HBox();
        HBox electricityValueHbox = new HBox();

        HBox elecConsumptionHbox = new HBox();
        HBox gasConsumptionHbox = new HBox();
        HBox waterConsumptionHbox = new HBox();

        Label gasActuatorFlavourText = new Label("Gas device value: ");
        gasActuatorValue = new Label();
        gasValueHbox.getChildren().addAll(gasActuatorFlavourText, gasActuatorValue);
        Label gasIsActiveFlavourText = new Label("Active: ");
        gasIsActiveValue = new Label();
        gasIsActiveHbox.getChildren().addAll(gasIsActiveFlavourText, gasIsActiveValue);
        gasVbox.getChildren().addAll(gasValueHbox, gasIsActiveHbox);

        Label waterActuatorFlavourText = new Label("Water device value: ");
        waterActuatorValue = new Label();
        waterValueHbox.getChildren().addAll(waterActuatorFlavourText, waterActuatorValue);
        Label waterIsActiveFlavourText = new Label("Active: ");
        waterIsActiveValue = new Label();
        waterIsActiveHbox.getChildren().addAll(waterIsActiveFlavourText, waterIsActiveValue);
        waterVbox.getChildren().addAll(waterValueHbox, waterIsActiveHbox);

        Label electricityActuatorFlavourText = new Label("Electricity device value: ");
        electricityActuatorValue = new Label();
        electricityValueHbox.getChildren().addAll(electricityActuatorFlavourText, electricityActuatorValue);
        Label electricityIsActiveFlavourText = new Label("Active: ");
        electricityIsActiveValue = new Label();
        electricityIsActiveHbox.getChildren().addAll(electricityIsActiveFlavourText, electricityIsActiveValue);
        electricityVbox.getChildren().addAll(electricityValueHbox, electricityIsActiveHbox);

        Label electricityConsumptionFlavourText = new Label("Total electricity Consumption: ");
        Label waterConsumptionFlavourText = new Label("Total water Consumption: ");
        Label gasConsumptionFlavourText = new Label("Total gas Consumption: ");

        gasConsumptionValue = new Label();
        electricityConsumptionValue = new Label();
        waterConsumptionValue = new Label();

        elecConsumptionHbox.getChildren().addAll(electricityConsumptionFlavourText, electricityConsumptionValue);
        gasConsumptionHbox.getChildren().addAll(gasConsumptionFlavourText, gasConsumptionValue);
        waterConsumptionHbox.getChildren().addAll(waterConsumptionFlavourText, waterConsumptionValue);

        electricityVbox.getChildren().add(elecConsumptionHbox);
        gasVbox.getChildren().add(gasConsumptionHbox);
        waterVbox.getChildren().add(waterConsumptionHbox);

        gasTotalHbox.getChildren().add(gasVbox);
        waterTotalHbox.getChildren().add(waterVbox);
        electricityTotalHbox.getChildren().add(electricityVbox);

        waterTotalHbox.setVisible(false);
        gasTotalHbox.setVisible(false);
        electricityTotalHbox.setVisible(false);

        actuatorsVbox.getChildren().addAll(gasTotalHbox, waterTotalHbox, electricityTotalHbox);
    }

    public String getSelectedComponent() {
        return selectedComponent;
    }
}