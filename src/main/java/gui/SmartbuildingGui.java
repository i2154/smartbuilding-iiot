package gui;

import client.ClientHelper;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;
import server.HttpServer;
import client.MqttHelper;
import client.SmartbuildingClient;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The main class of the GUI
 */
public class SmartbuildingGui extends Application {
    private static final Logger logger = LoggerFactory.getLogger(SmartbuildingGui.class);

    private static final int SCREEN_WIDTH = 1440;
    private static final int SCREEN_HEIGHT = 900;

    FloorTabPane floorTabPane;

    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private ClientHelper clientHelper;

    private HttpServer httpServer;

    private class HttpService extends io.dropwizard.Application<Configuration> {
        private final ClientHelper clientHelper;
        private final MqttHelper mqttHelper;

        private HttpService(ClientHelper clientHelper, MqttHelper mqttHelper) {
            this.clientHelper = clientHelper;
            this.mqttHelper = mqttHelper;
        }

        @Override
        public void run(Configuration configuration, Environment environment) throws Exception {
            environment.jersey().register(new HttpServer(this.clientHelper, this.mqttHelper));
        }
    }

    /**
     * Start and setup the Smartbuilding Client
     * @throws MqttException if MqttClient have some problems
     */
    private void setupSmartbuildingClient() throws Exception {
        String BROKER_ADDRESS = "127.0.0.1";
        int BROKER_PORT = 1883;

        String clientId = UUID.randomUUID().toString();

        MqttClientPersistence persistence = new MemoryPersistence();

        IMqttClient mqttClient = new MqttClient(String.format("tcp://%s:%d", BROKER_ADDRESS, BROKER_PORT), clientId, persistence);

        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);

        mqttClient.connect(options);

        MqttHelper mqttHelper = new MqttHelper(mqttClient);
        mqttHelper.init();

        SmartbuildingClient smartbuildingClient = new SmartbuildingClient(mqttHelper);
        smartbuildingClient.init();

        this.clientHelper = smartbuildingClient.getClientHelper();

        HttpService httpService = new HttpService(this.clientHelper, mqttHelper);

        httpService.run("server", "configuration.yml");
    }

    /**
     * Class the setupSmartbuildingClient method
     * @throws Exception Exception of the Javafx application
     */
    @Override
    public void init() throws Exception {
        super.init();

        try {
            setupSmartbuildingClient();
        } catch (MqttException e) {
            logger.error("Cannot initialize the Smartbuilding Client! Msg: {}", e.getMessage());
        }
    }

    /**
     * Initialize the Basic layer of the GUI and set the FloorTabPane to manage various FloorTabs
     * @param primaryStage The stage of an Application of javafx
     */
    @Override
    public void start(Stage primaryStage) {
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.TOP_CENTER);

        this.floorTabPane = new FloorTabPane(SCREEN_WIDTH, SCREEN_HEIGHT, clientHelper);
        floorTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        vBox.getChildren().add(floorTabPane);

        Scene scene = new Scene(vBox);
        scene.getStylesheets().add("/dark-theme.css");

        primaryStage.setScene(scene);
        primaryStage.setWidth(SCREEN_WIDTH);
        primaryStage.setHeight(SCREEN_HEIGHT);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Smartbuilding-IoT");
        primaryStage.show();
        primaryStage.setOnCloseRequest(windowEvent -> System.exit(0));

        startUpdate();
    }

    /**
     * Update the GUI every 5 seconds, and call the update method of FloorTabPane to update every FloorTab
     */
    private void startUpdate() {
        scheduler.scheduleAtFixedRate(()
                -> Platform.runLater(()
                -> floorTabPane.updateTabs(this.clientHelper.getDevicesMap(), this.clientHelper.getPeopleSensorMap(), this.clientHelper.getPoliciesHashMap())),0, 5, TimeUnit.SECONDS);
    }

    public static void main(String[] args) {
        launch(args);
    }

}