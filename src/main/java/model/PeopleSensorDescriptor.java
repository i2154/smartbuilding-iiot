package model;

/**
 * Descriptor for People sensor
 */
public class PeopleSensorDescriptor {
    private String floorId;
    private int peopleIn;
    private int peopleOut;
    private int currentPeople;

    public PeopleSensorDescriptor() {
    }

    /**
     * Constructor
     * @param floorId String that represent the floorId of a Floor
     * @param peopleIn People entered the floor
     * @param peopleOut People exited the floor
     * @param currentPeople Current people in the floor
     */
    public PeopleSensorDescriptor(String floorId, int peopleIn, int peopleOut, int currentPeople) {
        this.floorId = floorId;
        this.peopleIn = peopleIn;
        this.peopleOut = peopleOut;
        this.currentPeople = currentPeople;
    }

    public String getFloorId() {
        return floorId;
    }

    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }

    public int getPeopleIn() {
        return peopleIn;
    }

    public void setPeopleIn(int peopleIn) {
        this.peopleIn = peopleIn;
    }

    public int getPeopleOut() {
        return peopleOut;
    }

    public void setPeopleOut(int peopleOut) {
        this.peopleOut = peopleOut;
    }

    public int getCurrentPeople() {
        return currentPeople;
    }

    public void setCurrentPeople(int currentPeople) {
        this.currentPeople = currentPeople;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PeopleSensorDescriptor{");
        sb.append("floorId='").append(floorId).append('\'');
        sb.append(", peopleIn=").append(peopleIn);
        sb.append(", peopleOut=").append(peopleOut);
        sb.append(", currentPeople=").append(currentPeople);
        sb.append('}');
        return sb.toString();
    }
}