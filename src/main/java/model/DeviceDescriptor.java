package model;

/**
 * Descriptor for a generic device
 */
public class DeviceDescriptor {
    private String floorId;
    private String componentId;
    private String deviceId;
    private Boolean isEnabled;

    public DeviceDescriptor() {
    }

    /**
     * Constructor
     * @param floorId String that represent the floorId of a Floor
     * @param componentId String that represent the componentId of a Device
     * @param deviceId String that represent the unique Id for the Device
     * @param isEnabled If a device is active or not
     */
    public DeviceDescriptor(String floorId, String componentId, String deviceId, Boolean isEnabled) {
        this.floorId = floorId;
        this.componentId = componentId;
        this.deviceId = deviceId;
        this.isEnabled = isEnabled;
    }

    public String getFloorId() {
        return floorId;
    }

    public void setFloorId(String floorId) {
        this.floorId = floorId;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DeviceDescriptor{");
        sb.append("floorId='").append(floorId).append('\'');
        sb.append(", componentId='").append(componentId).append('\'');
        sb.append(", deviceId=").append(deviceId);
        sb.append(", isEnabled=").append(isEnabled);
        sb.append('}');
        return sb.toString();
    }
}