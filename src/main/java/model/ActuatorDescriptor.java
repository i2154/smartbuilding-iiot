package model;

/**
 * Descriptor for a generic actuator
 */
public class ActuatorDescriptor {
    private DeviceDescriptor deviceDescriptor;

    private double value;

    private String unit;

    public ActuatorDescriptor() {
    }

    /**
     * Constructor
     * @param deviceDescriptor Descriptor for generic device (@see DeviceDescriptor)
     * @param value Value of an actuator in a specific time
     * @param unit The unit of the actuator
     */
    public ActuatorDescriptor(DeviceDescriptor deviceDescriptor, double value, String unit) {
        this.deviceDescriptor = deviceDescriptor;
        this.value = value;
        this.unit = unit;
    }

    public DeviceDescriptor getDeviceDescriptor() {
        return deviceDescriptor;
    }

    public void setDeviceDescriptor(DeviceDescriptor deviceDescriptor) {
        this.deviceDescriptor = deviceDescriptor;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ActuatorDescriptor{");
        sb.append("deviceDescriptor=").append(deviceDescriptor.toString());
        sb.append(", value=").append(value);
        sb.append(", unit='").append(unit).append('\'');
        sb.append('}');
        return sb.toString();
    }
}