package model;

/**
 * Descriptor of the Policy applied for every Actuator
 */
public class PolicyDescriptor {
    private Boolean isEnabled;
    private double maxConsume;
    private long timeToReactivate;
    private Boolean timerFinished;

    public PolicyDescriptor() {
        this.isEnabled = true;
        this.maxConsume = 50.0;
        this.timeToReactivate = 100;
        this.timerFinished = false;
    }

    /**
     * Constructor
     * @param isEnabled If the Policy is enabled for a specific actuator
     * @param maxConsume The max amount of value that an actuator must surpass for activate the policy
     * @param timeToReactivate The time amount for reactivate an actuator
     */
    public PolicyDescriptor(Boolean isEnabled, double maxConsume, long timeToReactivate) {
        this.isEnabled = isEnabled;
        this.maxConsume = maxConsume;
        this.timeToReactivate = timeToReactivate;
        this.timerFinished = false;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public double getMaxConsume() {
        return maxConsume;
    }

    public void setMaxConsume(double maxConsume) {
        this.maxConsume = maxConsume;
    }

    public long getTimeToReactivate() {
        return timeToReactivate;
    }

    public void setTimeToReactivate(long timeToReactivate) {
        this.timeToReactivate = timeToReactivate;
    }

    public Boolean getTimerFinished() {
        return timerFinished;
    }

    public void setTimerFinished(Boolean timerFinished) {
        this.timerFinished = timerFinished;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PolicyDescriptor{");
        sb.append("isEnabled=").append(isEnabled);
        sb.append(", maxConsume=").append(maxConsume);
        sb.append(", timeToReactivate=").append(timeToReactivate);
        sb.append('}');
        return sb.toString();
    }
}